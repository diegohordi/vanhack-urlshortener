package tech.diegohordi.urlshortener.settings;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * <p>OAS Boostrap class.</p>
 * 
 * <p>This class boostraps Swagger as implementation for OAS Specification.</p>
 * 
 * @see https://swagger.io/specification/ 
 * 
 * @author diego.hordi
 */
@Configuration
@EnableSwagger2
public class OASBootstrap extends WebMvcConfigurationSupport {

	/**
	 * <p>Creates an instance of {@link ApiInfo} with this API basic info.</p>
	 * 
	 * @return An instance of {@link ApiInfo}.
	 */
	private ApiInfo getApiInfo() {
		return new ApiInfo("VanHack URL Shortener Challenge API", "VanHack URL Shortener Challenge API", "1.0.0", "Terms of Service", new Contact("Diego Hordi", "www.diegohordi.tech", "diego.hordi@gmail.com"), "License", "License", Collections.emptyList());
	}

	/**
	 * <p>Sets a list of default responses to the API.</p>
	 * 
	 * @return List of default responses.
	 */
	private List<ResponseMessage> getApiResponses() {
		List<ResponseMessage> responses = new ArrayList<ResponseMessage>();
		responses.add(new ResponseMessageBuilder().code(HttpStatus.INTERNAL_SERVER_ERROR.value()).message(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase()).build());
		return responses;
	}

	/**
	 * <p>Creates an instance of {@link Docket}.</p>
	 * 
	 * @return An instance of {@link Docket}.
	 */
	@Bean
	public Docket getApiDocumentation() {
		List<ResponseMessage> responses = getApiResponses();
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(getApiInfo()).select()
				.apis(RequestHandlerSelectors.basePackage("tech.diegohordi"))
				.paths(PathSelectors.ant("/api/**")).build()
				.useDefaultResponseMessages(Boolean.FALSE)
				.globalResponseMessage(RequestMethod.GET, responses)
				.globalResponseMessage(RequestMethod.POST, responses)
				.globalResponseMessage(RequestMethod.PUT, responses)
				.globalResponseMessage(RequestMethod.DELETE, responses);
	}	

	/*
	 * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport#addResourceHandlers(org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry)
	 */
	@Override
	protected void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/swagger-ui.html");
		registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
	}

}
