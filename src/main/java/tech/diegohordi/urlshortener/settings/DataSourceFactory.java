package tech.diegohordi.urlshortener.settings;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 * <p>{@link DataSource} bean factory.</p>
 * 
 * <p>Due to the lack of Spring Boot default support for the SQLite database, we need to create a {@link DataSource} bean
 * based on <b>application.properties</b> file.</p>
 * 
 * @author diego.hordi
 */
@Configuration
public class DataSourceFactory {

	/**
	 * Database URL.
	 */
	@Value("${spring.datasource.url}")
	private String datasourceUrl;

	/**
	 * Database driver class name.
	 */
	@Value("${spring.database.driverClassName}")
	private String dbDriverClassName;

	/**
	 * Database user name.
	 */
	@Value("${spring.datasource.username}")
	private String dbUsername;

	/**
	 * Database user name password.
	 */
	@Value("${spring.datasource.password}")
	private String dbPassword;

	/**
	 * <p>Creates an instance of {@link DataSource} based on <b>application.properties</b> settings file.</p>
	 * 
	 * @return An instance of {@link DataSource}.
	 */
	@Bean
	public DataSource getDataSource() {
		final DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(dbDriverClassName);
		dataSource.setUrl(datasourceUrl);
		dataSource.setUsername(dbUsername);
		dataSource.setPassword(dbPassword);
		return dataSource;
	}
	

}