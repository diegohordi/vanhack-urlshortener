package tech.diegohordi.urlshortener.model.entities;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <p>Entity that represents a shortened URL.</p>
 * 
 * @author diego.hordi
 */
@Entity
@Table(name = "tb01_url")
public class URLDictionary {

	/**
	 * ID of URL.
	 */
	@Id
	@Column(name = "url_id", nullable = false)
	private String id;

	/**
	 * Address of the shortened URL.
	 */
	@Column(name = "url_address", nullable = false)
	private String address;

	/**
	 * Date of shortening request.
	 */
	@Column(name = "url_shortening_date")
	private Timestamp shorteningDate;
	
	/**
	 * Number of hits to the URL.
	 */
	@Column(name = "url_hits")
	private BigInteger hits;	

	/**
	 * Last access date to the URL.
	 */
	@Column(name = "url_last_access_date")
	private Timestamp lastAccessDate;

	/**
	 * <p>Default constructor.</p>
	 */
	public URLDictionary() {
	}
	
	/**
	 * @param id ID of URL.
	 * @param address Address of the shortened URL.
	 */
	public URLDictionary(String id, String address) {
		super();
		this.id = id;
		this.address = address;
		this.shorteningDate = new Timestamp(Calendar.getInstance().getTimeInMillis());
		this.hits = BigInteger.ZERO;
		this.lastAccessDate = new Timestamp(Calendar.getInstance().getTimeInMillis());
	}

	/**
	 * @return Gets the value of {@link URLDictionary#id}.
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id Sets the value of {@link URLDictionary#id}.
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return Gets the value of {@link URLDictionary#address}.
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address Sets the value of {@link URLDictionary#address}.
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return Gets the value of {@link URLDictionary#shorteningDate}.
	 */
	public Timestamp getShorteningDate() {
		return shorteningDate;
	}

	/**
	 * @param shorteningDate Sets the value of {@link URLDictionary#shorteningDate}.
	 */
	public void setShorteningDate(Timestamp shorteningDate) {
		this.shorteningDate = shorteningDate;
	}

	/**
	 * @return Gets the value of {@link URLDictionary#hits}.
	 */
	public BigInteger getHits() {
		return hits;
	}

	/**
	 * @param hits Sets the value of {@link URLDictionary#hits}.
	 */
	public void setHits(BigInteger hits) {
		this.hits = hits;
	}

	/**
	 * @return Gets the value of {@link URLDictionary#lastAccessDate}.
	 */
	public Timestamp getLastAccessDate() {
		return lastAccessDate;
	}

	/**
	 * @param lastAccessDate Sets the value of {@link URLDictionary#lastAccessDate}.
	 */
	public void setLastAccessDate(Timestamp lastAccessDate) {
		this.lastAccessDate = lastAccessDate;
	}

}
