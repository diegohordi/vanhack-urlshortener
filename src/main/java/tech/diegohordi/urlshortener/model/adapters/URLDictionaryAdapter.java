package tech.diegohordi.urlshortener.model.adapters;

import tech.diegohordi.urlshortener.model.entities.URLDictionary;
import tech.diegohordi.urlshortener.model.to.URLDictionaryTO;

/**
 * <p>Adapter used to convert {@link URLDictionary} entity into an {@link URLDictionaryTO} transfer object.</p>
 * 
 * @author diego.hordi
 */
public class URLDictionaryAdapter {

	/**
	 * <p>Convert the given entity into an {@link URLDictionaryTO} transfer object.</p>
	 * 
	 * @param entity Entity that will be converted.
	 * @return {@link URLDictionaryTO} converted.
	 */
	public static URLDictionaryTO adapt( URLDictionary entity ) {
		URLDictionaryTO to = new URLDictionaryTO();
		to.setId(entity.getId());
		to.setAddress(entity.getAddress());
		to.setHits(entity.getHits());
		to.setLastAccessDate(entity.getLastAccessDate());
		to.setShorteningDate(entity.getShorteningDate());
		return to;
	}
	
}
