package tech.diegohordi.urlshortener.model.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tech.diegohordi.urlshortener.model.entities.URLDictionary;

/**
 * <p>Repository of {@link URLDictionary} entities.</p>
 * 
 * @author diego.hordi
 */
@Repository
public interface URLDictionaryRepository extends JpaRepository<URLDictionary, String>{

}
