package tech.diegohordi.urlshortener.model.to;

import java.math.BigInteger;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;

/**
 * <p>Transfer object used to expose stats from URL Shortener API.</p>
 * 
 * @author diego.hordi
 */
@ApiModel("Stats")
public class StatsTO {

	/**
	 * Amount of shortened URL.
	 */
	@ApiParam("Amount of shortened URL")
	private BigInteger amount;

	/**
	 * <p>Default constructor.</p>
	 */
	public StatsTO() {
	}

	/**
	 * @return Gets the value of {@link StatsTO#amount}.
	 */
	public BigInteger getAmount() {
		return amount;
	}

	/**
	 * @param amount Sets the value of {@link StatsTO#amount}.
	 */
	public void setAmount(BigInteger amount) {
		this.amount = amount;
	}

}
