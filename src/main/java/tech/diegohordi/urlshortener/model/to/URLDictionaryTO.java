package tech.diegohordi.urlshortener.model.to;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Calendar;

import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;

/**
 * <p>Transfer object that represents a URL shortened.</p>
 * 
 * @author diego.hordi
 */
@ApiModel("URL Data")
public class URLDictionaryTO {

	/**
	 * ID of URL.
	 */
	@ApiParam("ID of URL")
	private String id;

	/**
	 * Address of the shortened URL.
	 */
	@ApiParam("Address of the shortened URL")
	private String address;

	/**
	 * Date of shortening request.
	 */
	@ApiParam("Date of shortening request")
	@JsonFormat(shape=Shape.STRING,pattern="yyyy-MM-dd HH:mm:ss")
	private Timestamp shorteningDate;
	
	/**
	 * Number of hits to the URL.
	 */
	@Column(name = "url_hits")
	@ApiParam("Number of hits to the URL")
	private BigInteger hits;	

	/**
	 * Last access date to the URL.
	 */
	@ApiParam("Last access date to the URL")
	@JsonFormat(shape=Shape.STRING,pattern="yyyy-MM-dd HH:mm:ss")
	private Timestamp lastAccessDate;

	/**
	 * <p>Default constructor.</p>
	 */
	public URLDictionaryTO() {
	}
	
	/**
	 * @param id ID of URL.
	 * @param address Address of the shortened URL.
	 */
	public URLDictionaryTO(String id, String address) {
		super();
		this.id = id;
		this.address = address;
		this.shorteningDate = new Timestamp(Calendar.getInstance().getTimeInMillis());
	}

	/**
	 * @return Gets the value of {@link URLDictionaryTO#id}.
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id Sets the value of {@link URLDictionaryTO#id}.
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return Gets the value of {@link URLDictionaryTO#address}.
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address Sets the value of {@link URLDictionaryTO#address}.
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return Gets the value of {@link URLDictionaryTO#shorteningDate}.
	 */
	public Timestamp getShorteningDate() {
		return shorteningDate;
	}

	/**
	 * @param shorteningDate Sets the value of {@link URLDictionaryTO#shorteningDate}.
	 */
	public void setShorteningDate(Timestamp shorteningDate) {
		this.shorteningDate = shorteningDate;
	}

	/**
	 * @return Gets the value of {@link URLDictionaryTO#hits}.
	 */
	public BigInteger getHits() {
		return hits;
	}

	/**
	 * @param hits Sets the value of {@link URLDictionaryTO#hits}.
	 */
	public void setHits(BigInteger hits) {
		this.hits = hits;
	}

	/**
	 * @return Gets the value of {@link URLDictionaryTO#lastAccessDate}.
	 */
	public Timestamp getLastAccessDate() {
		return lastAccessDate;
	}

	/**
	 * @param lastAccessDate Sets the value of {@link URLDictionaryTO#lastAccessDate}.
	 */
	public void setLastAccessDate(Timestamp lastAccessDate) {
		this.lastAccessDate = lastAccessDate;
	}

}
