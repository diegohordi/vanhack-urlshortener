package tech.diegohordi.urlshortener;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * <p>Main setting class of this Spring Boot application.</p>
 *  
 * @author diego.hordi
 */
@SpringBootApplication
@EntityScan(basePackages = { "tech.diegohordi.urlshortener.model.entities" })
@ComponentScan(basePackages = { "tech.diegohordi.urlshortener" })
@EnableJpaRepositories(basePackages = { "tech.diegohordi.urlshortener.model.repositories" })
@EnableTransactionManagement
public class URLShortenerApp {
	
	/**
	 * <p>Bootstraps the URL Shortener App.</p>
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(URLShortenerApp.class, args);
	}
	
}
