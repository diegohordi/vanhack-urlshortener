package tech.diegohordi.urlshortener.controllers.rest.model.to;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel("Request to short URL")
public class URLRequest {

	@ApiModelProperty("URL to be shorten")
	private String url;
	
	public URLRequest() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return Get the value of {@link URLRequest#url}.
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url Set the value of {@link URLRequest#url}.
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	
	
	
}
