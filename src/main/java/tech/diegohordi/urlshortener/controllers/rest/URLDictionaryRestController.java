package tech.diegohordi.urlshortener.controllers.rest;

import javax.servlet.http.HttpServletResponse;

import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import tech.diegohordi.urlshortener.controllers.bo.URLDictionaryBO;
import tech.diegohordi.urlshortener.controllers.rest.model.to.URLRequest;
import tech.diegohordi.urlshortener.model.to.StatsTO;
import tech.diegohordi.urlshortener.model.to.URLDictionaryTO;

@RestController
@Api("URL Shortener REST API")
@RequestMapping("/api/url")
public class URLDictionaryRestController {

	@Autowired
	private URLDictionaryBO urlDictionaryBO;
	
	private URLDictionaryBO getUrlDictionaryBO() {
		return urlDictionaryBO;
	}
	
	@ApiOperation("Short the given URL")
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody String getShortUrl(
			@RequestBody(required=true) @ApiParam(required=true,value="URL") URLRequest urlRequest
			) throws ServiceException {
		return getUrlDictionaryBO().getShortUrl(urlRequest.getUrl());
	}
	
	@ApiOperation("Searches for the given URL unique ID and redirects to it")
	@RequestMapping(path="/{id}", method = RequestMethod.GET)
	public @ResponseBody void getURLFromUniqueID(
			@PathVariable(value="id",required=true) String uniqueId,
			HttpServletResponse response
			) throws ServiceException {
		String url = getUrlDictionaryBO().getURLFromUniqueID(uniqueId);
		response.addHeader("Location", url);
	    response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
	}
	
	@ApiOperation("Searches for the given unique ID and retrieves your statistics data")
	@RequestMapping(path="/{id}/stats", method = RequestMethod.GET)
	public @ResponseBody URLDictionaryTO getURLStatsFromUniqueID(
			@PathVariable(value="id",required=true) @ApiParam("Unique ID") String uniqueId
			) throws ServiceException {
		return getUrlDictionaryBO().getURLStatsFromUniqueID(uniqueId);
	}
	
	@ApiOperation("Gets stats from URL Shortener API")
	@RequestMapping(path="/stats", method = RequestMethod.GET)
	public @ResponseBody StatsTO getStats(
			) throws ServiceException {
		return getUrlDictionaryBO().getStats();
	}
	
}
