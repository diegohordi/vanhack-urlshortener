package tech.diegohordi.urlshortener.controllers.bo;

import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tech.diegohordi.urlshortener.controllers.exceptions.APIErrors;
import tech.diegohordi.urlshortener.controllers.exceptions.ServiceException;
import tech.diegohordi.urlshortener.model.adapters.URLDictionaryAdapter;
import tech.diegohordi.urlshortener.model.entities.URLDictionary;
import tech.diegohordi.urlshortener.model.repositories.URLDictionaryRepository;
import tech.diegohordi.urlshortener.model.to.StatsTO;
import tech.diegohordi.urlshortener.model.to.URLDictionaryTO;

/**
 * <p>Business object that is responsible to short and to get URL addresses.</p>
 * 
 * @see https://www.geeksforgeeks.org/how-to-design-a-tiny-url-or-url-shortener/
 * 
 * @author diego.hordi
 */
@Transactional
@Service
public class URLDictionaryBO {
	
	/**
	 * Possible chars.
	 */
	private static final char[] CHAR_TABLE = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
	
	/**
	 * Repository of {@link URLDictionary} entities.
	 */
	@Autowired
	private URLDictionaryRepository repository;
	
	/**
	 * @return Repository of {@link URLDictionary} entities.
	 */
	private URLDictionaryRepository getRepository() {
		return repository;
	}
	
	/**
	 * <p>Validates the given URL and throw the {@link APIErrors#INVALID_URL} exception cas if the URL invalid.</p>
	 * 
	 * @param url URL that will be validated.
	 * @throws ServiceException
	 * <ul>
	 * 		<li>{@link APIErrors#INVALID_URL} - If the given URL is invalid.</li>
	 * </ul>
	 */
	public void validateURL( String url ) throws ServiceException{
		try {
			new URL(url);
		} catch (MalformedURLException e) {
			throw new ServiceException(APIErrors.INVALID_URL);
		}
	}
	
	/**
	 * <p>Short the given URL.</p>
	 * 
	 * <p>To shorten the given URL this object uses the {@link String#hashCode()} to get its hash code as base 
	 * to the process.</p>
	 * 
	 * @param url URL that will be shorten.
	 * @return Shortened URL.
	 */
	public String getShortUrl( String url ) throws ServiceException{
		validateURL(url);
		int hash = url.hashCode();
		hash = ( hash < 0 ) ? hash*(-1) : hash;
		String uniqueId = createUniqueID(new BigInteger(String.valueOf(hash)));
		getRepository().save(new URLDictionary(uniqueId, url));
		return uniqueId;
	}
	
	/**
	 * <p>Searchs for the given unique ID to get the URL address.</p>
	 * 
	 * @param uniqueID Unique ID to be searched.
	 * @return URL address from the given unique ID.
	 * @throws ServiceException
	 * <ul>
	 * 		<li>{@link APIErrors#URL_NOT_FOUND} - If the given unique ID was not found.</li>
	 * </ul>
	 */
	public String getURLFromUniqueID( String uniqueID ) throws ServiceException{
		if( getRepository().existsById(uniqueID) ) {
			URLDictionary urlDictionary = getRepository().findById(uniqueID).get();
			urlDictionary.setHits( urlDictionary.getHits().add(BigInteger.ONE));
			urlDictionary.setLastAccessDate(new Timestamp(Calendar.getInstance().getTimeInMillis()));
			urlDictionary = getRepository().saveAndFlush(urlDictionary);
			return urlDictionary.getAddress();
		} else {
			throw new ServiceException(APIErrors.URL_NOT_FOUND);
		}
	}
	
	/**
	 * <p>Searches for the given unique ID and retrieves your statistics data.</p>
	 * 
	 * @param uniqueID Unique ID to be searched.
	 * @return URL address from the given unique ID.
	 * @throws ServiceException
	 * <ul>
	 * 		<li>{@link APIErrors#URL_NOT_FOUND} - If the given unique ID was not found.</li>
	 * </ul>
	 */
	public URLDictionaryTO getURLStatsFromUniqueID( String uniqueID ) throws ServiceException{
		if( getRepository().existsById(uniqueID) ) {
			URLDictionary urlDictionary = getRepository().getOne(uniqueID);
			return URLDictionaryAdapter.adapt(urlDictionary);
		} else {
			throw new ServiceException(APIErrors.URL_NOT_FOUND);
		}
	}
	
	/**
	 * <p>Creates an unique ID based on the hash code of the given URL to be shorten.</p>
	 * 
	 * @param id Long number based on the hash code.
	 * @return Unique ID based on list of possible chars.
	 */
	private String createUniqueID(BigInteger id) {
	    StringBuilder uniqueId = new StringBuilder();
	    for (BigInteger digit : convertToBase62(id)) {
	        uniqueId.append(CHAR_TABLE[digit.intValue()]);
	    }
	    return uniqueId.toString();
	}
	
	/**
	 * <p>Converts every digit of the given ID to a base 62 number.</p>
	 * 
	 * @param id ID that will be converted.
	 * @return List of digits converted to base 62 numbers.
	 */
	private List<BigInteger> convertToBase62(BigInteger id) {
		LinkedList<BigInteger> digits = new LinkedList<BigInteger>();
		while (id.compareTo(BigInteger.ZERO) > 0) {
	        BigInteger remainder = new BigInteger(String.valueOf(id)).remainder(new BigInteger("62"));
	        id = id.divide(new BigInteger("62"));
	        digits.addFirst(remainder);
	    }
	    return digits;
	}
	

	
	/**
	 * <p>Gets stats from URL Shortener API.</p>
	 * 
	 * @return Stats.
	 */
	public StatsTO getStats() {
	  StatsTO stats = new StatsTO();
	  stats.setAmount(new BigInteger( String.valueOf(getRepository().count())));
	  return stats;
	}
	
}
