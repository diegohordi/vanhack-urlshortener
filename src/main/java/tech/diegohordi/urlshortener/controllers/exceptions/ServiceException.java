package tech.diegohordi.urlshortener.controllers.exceptions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * Exceção que deve encapsular todas as demais exceções da camada de serviços.
 * </p>
 * 
 * @author diego.hordi
 */
public class ServiceException extends RuntimeException {

	private static final long serialVersionUID = 7986864620634914985L;

	/**
	 * Error code.
	 */
	private ErrorCode code;

	/**
	 * Exception parameters.
	 */
	private List<Object> params;

	/**
	 * @param code Error code.
	 * @param params Exception parameters.
	 */
	public ServiceException(ErrorCode code, Object... params) {
		this.code = code;
		this.params = Arrays.asList(params);
	}

	/**
	 * @param code Error code.
	 */
	public ServiceException(ErrorCode code) {
		this.code = code;
	}

	/**
	 * @param excecao Exceção que será encapsulada.
	 */
	public ServiceException(final Throwable excecao) {
		super(excecao);
	}

	/**
	 * @return Get the value of {@link ServiceException#code}.
	 */
	public ErrorCode getCode() {
		return code;
	}

	/**
	 * @param code Set the value of {@link ServiceException#code}.
	 */
	public void setCode(ErrorCode code) {
		this.code = code;
	}

	/**
	 * @return Get the value of {@link ServiceException#params}.
	 */
	public List<Object> getParams() {
		if( params == null ) {
			params = new ArrayList<Object>();
		}
		return params;
	}

	/**
	 * @param params Set the value of {@link ServiceException#params}.
	 */
	public void setParams(List<Object> params) {
		this.params = params;
	}

}
