package tech.diegohordi.urlshortener.controllers.exceptions;

import org.springframework.http.HttpStatus;

/**
 * <p>Enumeration of all exceptions with their respective HTTP Status.</p>
 * 
 * @author diego.hordi
 */
public enum APIErrors implements ErrorCode {

	INVALID_URL("INVALID_URL", 400),
	URL_NOT_FOUND("URL_NOT_FOUND", 404),
	UNEXPECTED_ERROR("UNEXPECTED_ERROR", 500);

	/**
	 * Error code.
	 */
	private final String code;
	
	/**
	 * {@link HttpStatus} associated.
	 */
	private final Integer status;

	/**
	 * <p>Default construtor.</p>
	 * 
	 * @param code Error code.
	 * @param status {@link HttpStatus} associated.
	 */
	private APIErrors(final String code, final Integer status) {
		this.code = code;
		this.status = status;
	}

	/*
	 * @see tech.diegohordi.urlshortener.controllers.exceptions.CodigoErro#getCode()
	 */
	public String getCode() {
		return code;
	}

	/*
	 * @see tech.diegohordi.urlshortener.controllers.exceptions.CodigoErro#getStatus()
	 */
	public Integer getStatus() {
		return status;
	}

	/*
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return code;
	}
}
