package tech.diegohordi.urlshortener.controllers.exceptions;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

/**
 * <p>Encapsulates specific attributes of the error messages handled by this
 * application.</p>
 * 
 * @author diego.hordi
 */
public class ErrorMessage implements Serializable {

	private static final long serialVersionUID = 4878825827657916191L;

	/**
	 * Error code.
	 */
	@ApiModelProperty(value = "Error code")
	private String code;

	/**
	 * HTTP Status.
	 */
	@ApiModelProperty(value = "HTTP Status")
	private Integer status;

	/**
	 * Error description.
	 */
	@ApiModelProperty(value = "Error description")
	private String error;

	/**
	 * Error message.
	 */
	@ApiModelProperty(value = "Error message")
	private String message;

	/**
	 * Error params.
	 */
	@ApiModelProperty(value = "Error params")
	private Object[] params;
	
	/**
	 * Default constructor.
	 */
	public ErrorMessage() {
	}

	/**
	 * @return Get the value of {@link ErrorMessage#code}.
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code Set the value of {@link ErrorMessage#code}.
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return Get the value of {@link ErrorMessage#status}.
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status Set the value of {@link ErrorMessage#status}.
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return Get the value of {@link ErrorMessage#error}.
	 */
	public String getError() {
		return error;
	}

	/**
	 * @param error Set the value of {@link ErrorMessage#error}.
	 */
	public void setError(String error) {
		this.error = error;
	}

	/**
	 * @return Get the value of {@link ErrorMessage#message}.
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message Set the value of {@link ErrorMessage#message}.
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return Get the value of {@link ErrorMessage#params}.
	 */
	public Object[] getParams() {
		return params;
	}

	/**
	 * @param params Set the value of {@link ErrorMessage#params}.
	 */
	public void setParams(Object[] params) {
		this.params = params;
	}

}
