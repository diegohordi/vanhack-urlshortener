package tech.diegohordi.urlshortener.controllers.exceptions;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * <p>Class responsible to handle all exceptions thrown by this application's REST Controllers.</p>
 * 
 * @author diego.hordi
 */
@ControllerAdvice
public class ResponseExceptionHandler extends ResponseEntityExceptionHandler {

	/**
	 * Message sources (message.properties).
	 */
	@Autowired
	private MessageSource messagesSource;
	
	/**
	 * @return Retorna o código de erro aplicável à exceções não esperadas.
	 */
	protected ErrorCode getUnexpectedErrorCode() {
		return APIErrors.UNEXPECTED_ERROR;
	}
	
	/**
	 * <p>Get the error code description.</p>
	 * 
	 * @param code Error code associated to the given message.
	 * @param params Exception params if exists.
	 * @return Message associated to the given error.
	 */
	private String getErrorDescription(final ErrorCode code, final Object... params) {
		return messagesSource.getMessage(code.toString(), params, LocaleContextHolder.getLocale());
	}
	
	/**
	 * <p>Method responsible to handle {@link ServiceException} exceptions.</p>
	 * 
	 * @param exception Exception that will be handled.
	 * @return Error message that will be returned.
	 */
	@ExceptionHandler({ ServiceException.class })
	public ResponseEntity<ErrorMessage> handleServiceException(final ServiceException exception) {
		ResponseEntity<ErrorMessage> response = null;
		if (exception.getCode() != null) {
			String message = getErrorDescription(exception.getCode(), exception.getParams());
			logger.error(message, exception);
			response = getErrorMessage(exception.getCode(), message, exception.getParams().toArray());
		} else if (exception.getCause() != null) {
			response = handleException(exception.getCause());
		}
		return response;
	}

	/**
	 * <p>Method responsible to handle {@link RuntimeException} and {@link Exception} exceptions.</p>
	 * 
	 * @param exception Exception that will be handled.
	 * @return Error message that will be returned.
	 */
	@ExceptionHandler({ RuntimeException.class, Exception.class })
	public ResponseEntity<ErrorMessage> handleException(final Throwable exception) {
		ErrorCode code = getUnexpectedErrorCode();
		String message = getErrorDescription(code, exception.getMessage());
		logger.error(message, exception);
		return getErrorMessage(code, message, null);
	}

	/**
	 * <p>Method responsible to handle {@link ConstraintViolationException} exceptions.</p>
	 * 
	 * @param exception Exception that will be handled.
	 * @return Error message that will be returned.
	 */
	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<ErrorMessage> handleException(final ConstraintViolationException exception) {
		ErrorMessage response = new ErrorMessage();
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		response.setError(HttpStatus.BAD_REQUEST.getReasonPhrase());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(response);
	}

	/*
	 * @see org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler#handleMethodArgumentNotValid(org.springframework.web.bind.MethodArgumentNotValidException, org.springframework.http.HttpHeaders, org.springframework.http.HttpStatus, org.springframework.web.context.request.WebRequest)
	 */
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException e, HttpHeaders headers, HttpStatus status, final WebRequest request) {
		ErrorMessage response = new ErrorMessage();
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		response.setError(HttpStatus.BAD_REQUEST.getReasonPhrase());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(response);
	}

	/*
	 * @see org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler#handleMissingServletRequestParameter(org.springframework.web.bind.MissingServletRequestParameterException, org.springframework.http.HttpHeaders, org.springframework.http.HttpStatus, org.springframework.web.context.request.WebRequest)
	 */
	@Override
	protected ResponseEntity<Object> handleMissingServletRequestParameter( MissingServletRequestParameterException e, HttpHeaders headers, HttpStatus status, WebRequest request) {
		ErrorMessage response = new ErrorMessage();
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		response.setError(HttpStatus.BAD_REQUEST.getReasonPhrase());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(response);
	}

	/**
	 * <p>Sets an error message according to the given parameters.</p>
	 * 
	 * @param code Error code.
	 * @param message Error message.
	 * @param params Error params if exists.
	 * @return Error message.
	 */
	private ResponseEntity<ErrorMessage> getErrorMessage(ErrorCode code, String message, Object[] params) {
		ErrorMessage response = new ErrorMessage();
		response.setCode(code.getCode());
		response.setError(code.toString());
		response.setParams(params);
		response.setMessage(message);
		HttpStatus status = HttpStatus.valueOf(code.getStatus());
		response.setError(status.getReasonPhrase());
		response.setStatus(status.value());
		return ResponseEntity.status(status).body(response);
	}

}
