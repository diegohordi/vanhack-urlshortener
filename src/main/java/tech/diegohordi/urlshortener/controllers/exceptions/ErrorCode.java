package tech.diegohordi.urlshortener.controllers.exceptions;

import org.springframework.http.HttpStatus;

/**
 * <p>Interface that declares the necessary methods to expose error messages.</p>
 * 
 * @author diego.hordi
 */
public interface ErrorCode {

	/**
	 * @return Error code.
	 */
	public String getCode();

	/**
	 * @return {@link HttpStatus} associated.
	 */
	public Integer getStatus();

}